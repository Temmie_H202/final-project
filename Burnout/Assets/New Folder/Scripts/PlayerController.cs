﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class PlayerController : MonoBehaviour
{
    Rigidbody2D rB2D;


    public float runspeed;
    public float jumpforce;
    public Text HealthText;

    public static float health;

    
    public AudioSource keey;
    
    public SpriteRenderer SpriteRenderer;
    public Animator animator;
    private Rigidbody2D rbe;

    bool haskey = false;
    
    private int nextscene;
    private int currentscene;

    // Start is called before the first frame update
    void Start()
    {
        rbe = GetComponent<Rigidbody2D>();
        health = 40;

        SetHealthText();

        nextscene = SceneManager.GetActiveScene().buildIndex + 1;
        currentscene = SceneManager.GetActiveScene().buildIndex + 0;

        rB2D = GetComponent<Rigidbody2D>();

        
        
    }


    void SetHealthText() 
    {
        HealthText.text = "  Health: " + health.ToString();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            haskey = true;
            Debug.Log(haskey);
            keey.Play();
            Debug.Log("heardkey");



        }

        if (other.gameObject.CompareTag("door"))
        {
            
            if (haskey)
            {
                
                SceneManager.LoadScene(nextscene);
            }
        }

        if (other.gameObject.CompareTag("deathbox"))
        {
            SceneManager.LoadScene(currentscene);

        }

        if (other.gameObject.CompareTag("Endgame"))
        {
            SceneManager.LoadScene(nextscene);
        }
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {

            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                
                jump();

            }
           
        }

        if (Input.GetKeyDown(KeyCode.R))  
        {
            SceneManager.LoadScene(currentscene);
        }

        health -= 1.5f * Time.deltaTime;
        SetHealthText();

        if (health < 0)
        {
            SceneManager.LoadScene(currentscene);
        }

        

    }


    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runspeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
        {
        SpriteRenderer.flipX = false;
        }
        else if (rB2D.velocity.x < 0)
        {
            SpriteRenderer.flipX = true;
        }
        else
        {
         
        }

         


    }



    void jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpforce);
    }








}
